# UW Event Factuality Dataset #

## Introduction ##

This dataset contains annotations of text from the TempEval-3 corpus. The annotation is described in:

>[Event Detection and Factuality Assessment with Non-Expert Supervision](http://homes.cs.washington.edu/~kentonl/pub/lacz-emnlp.2015.pdf)

>[Kenton Lee](http://homes.cs.washington.edu/~kentonl), [Yoav Artzi](http://yoavartzi.com), [Yejin Choi](http://homes.cs.washington.edu/~yejin), and [Luke Zettlemoyer](http://homes.cs.washington.edu/~lsz)

>*In Proceedings of the Conference on Empirical Methods in Natural Language Processing (EMNLP), 2015*

The dataset is in the BRAT standoff format (http://brat.nlplab.org/standoff.html). It is split into train, development, and test sets. A visualization of the data can be found at http://appositive.cs.washington.edu/brat/index.xhtml#/fact

If you are looking for the code to replicate the experiment, see https://bitbucket.org/kentonl/factuality-experiments.

For questions and inquiries, please contact [Kenton Lee](mailto:kentonl@cs.washington.edu).
